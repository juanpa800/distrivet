$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 2000
	});
	$('#contacto').on('show.bs.modal', function(e){
		console.log('el mensaje de show');

		$('#contactoBtn').removeClass("btn-outline-primary");
		$('#contactoBtn').addClass("btn-success");
		$('#contactoBtn').prop('disabled', true);

	});
	$('#contacto').on('shown.bs.modal', function(e){
		console.log('el mensaje de shown');
	});
	$('#contacto').on('hide.bs.modal', function(e){
		console.log('el mensaje de hide');
	});
	$('#contacto').on('hidden.bs.modal', function(e){
		console.log('el mensaje de hidden');
		$('#contactoBtn').prop('disabled', false);
		$('#contactoBtn').removeClass("btn-success");
		$('#contactoBtn').addClass("btn-outline-primary");
	});
});